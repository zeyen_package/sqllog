<?php
namespace Zeyen\Sqllog;
use Illuminate\Database\Events\QueryExecuted;
use support\Db;
use support\Log;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Webman\Bootstrap;
/**
 * Nodes:
 * Author: Zeyen
 * Date: 2023/12/15
 * Email: <Zeyen@aliyun.com>
 * Class: SqlLog
 */
class SqlLog implements Bootstrap
{
    /**
     * 自定义输出格式，否则输出前面会带有当前文件，无用信息
     * @param $var
     * @return void
     */
    public static function dumpvar($var): void
    {
        $cloner = new VarCloner();
        $dumper = new CliDumper();
        $dumper->dump($cloner->cloneVar($var));
    }

    public static function start($worker)
    {
        $config = config('plugin.zeyen.sqllog.app');
        // Is it console environment ?
        $is_console = !$worker;
        // 命令行环境下
        if ($is_console) return;
        // 插件未启用状态
        if(!$config['enable']) return;

//        if (!config("app.debug") || config("app.debug") === 'false') return;

        $appPath = app_path();
        Db::connection()->listen(function (QueryExecuted $queryExecuted) use ($appPath,$config) {
            if (isset($queryExecuted->sql) and $queryExecuted->sql !== "select 1") {
                $bindings = $queryExecuted->bindings;
                $sql = array_reduce(
                    $bindings,
                    function ($sql, $binding) {
                        return preg_replace('/\?/', is_numeric($binding) ? $binding : "'" . $binding . "'", $sql, 1);
                    },
                    $queryExecuted->sql
                );
                $traces = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
                // 记录app目录下产生的所有sql语句
                foreach ($traces as $trace){
                    if (isset($trace['file']) && isset($trace["function"])) {
                        if (str_contains($trace['file'], $appPath)) {
                            if($config['mode']===2){
                                // 判断是否为继承类的方法，如果是则跳过
                                if (isset($trace['class']) && !str_contains($trace['class'], 'Illuminate\Database')) {
                                    continue;
                                }
                            }
                            $file = str_replace(base_path(), '', $trace['file']);
                            $str = "[file] {$file}:{$trace['line']} [function]:{$trace["function"]}";
                            if(config('app.debug') && $config['is_dump']){
                                // 调试模式下和is_dump开启状态，打印sql语句
                                self::dumpvar("[sql] [time:{$queryExecuted->time} ms] [{$sql}]");
                                self::dumpvar($str);
                                self::dumpvar('----------------------------------------------------------------');
                            }
                            //标记下慢查询
                            $level = 'info';
                            $remark ='SQL';
                            if($queryExecuted->time >$config['slow_time']??3000){
                                $level='warning';
                                $remark ='SOLWE QUERY SQL';
                            }
                            $channel = $config['log_channel']??array_key_first(config('log'));
                            if(isset(config('log')[$channel])){
                                Log::channel($channel)->$level("[sql] [time:{$queryExecuted->time} ms] [{$sql}]");
                                Log::channel($channel)->$level($str);
                                Log::channel($channel)->$level("----------------------------   $remark   ------------------------------------");
                            }
                            //开启慢查询记录
                            if($config['slow_log']){
                                if($queryExecuted->time >$config['slow_time']??3000){
                                    $slowChannel = $config['slow_sql_channel']??$channel;
                                    if(isset(config('log')[$slowChannel])){
                                        Log::channel($slowChannel)->warning("[sql] [time:{$queryExecuted->time} ms] [{$sql}]");
                                        Log::channel($slowChannel)->warning($str);
                                        Log::channel($slowChannel)->warning('---------------------------   SOLWE QUERY SQL  -------------------------');
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });
    }
}
