<?php

return [
    'enable' => true,
    'mode'=>2, // 1记录app目录下产生的sql，追溯子类 2:仅记录数据操作类产生的sql，不追链(默认)
    'log_channel' =>'default', // 日志记录渠道，默认是default，请确认系统是否有这个日志渠道，或添加其他渠道
    'is_dump'=>true, // 是否在控制台输出sql，方便调试，需要配个app debug = true 使用
    'slow_log'=>true, // 是否记录慢sql
    'slow_time'=>3000, // 记录慢sql的阀值，单位毫秒
    'slow_sql_channel' =>'sql_slow', // 记录慢sql的日志渠道，不填写或者未配置将使用sql日志渠道
];
